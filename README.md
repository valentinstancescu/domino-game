##Domino game in PHP
This is a program which allows two players to play Domino against each other:

- The 28 tiles are shuffled face down and form the stock. Each player draws seven tiles.
- Pick a random tile to start the line of play.
- The players alternately extend the line of play with one tile at one of its two ends;
- A tile may only be placed next to another tile, if their respective values on the
connecting ends are identical.
- If a player is unable to place a valid tile, they must keep on pulling tiles from the stock
until they can.
- The game ends when one player wins by playing their last tile.

```
   Game starting with first tile: <0:3>
   Alice plays <3:5> to connect it to tile <0:3> on the board
   The current board is: <0:3><3:5>
   Bob plays <0:1> to connect it to tile <0:3> on the board
   The current board is: <1:0><0:3><3:5>
   Alice plays <1:5> to connect it to tile <1:0> on the board
   The current board is: <5:1><1:0><0:3><3:5>
   Bob cant't play, drawing tile <0:2>
   Bob cant't play, drawing tile <0:6>
   Bob cant't play, drawing tile <6:6>
   Bob cant't play, drawing tile <2:3>
   Bob cant't play, drawing tile <5:5>
   Bob plays <5:5> to connect it to tile <5:1> on the board
   The current board is: <5:5><5:1><1:0><0:3><3:5>
   Alice plays <0:5> to connect it to tile <5:5> on the board
   The current board is: <0:5><5:5><5:1><1:0><0:3><3:5>
   Bob plays <0:0> to connect it to tile <0:5> on the board
   The current board is: <0:0><0:5><5:5><5:1><1:0><0:3><3:5>
   Alice plays <0:4> to connect it to tile <0:0> on the board
   The current board is: <4:0><0:0><0:5><5:5><5:1><1:0><0:3><3:5>
   Bob plays <1:4> to connect it to tile <4:0> on the board
   The current board is: <1:4><4:0><0:0><0:5><5:5><5:1><1:0><0:3><3:5>
   Alice plays <1:1> to connect it to tile <1:4> on the board
   The current board is: <1:1><1:4><4:0><0:0><0:5><5:5><5:1><1:0><0:3><3:5>
   Bob plays <1:6> to connect it to tile <1:1> on the board
   The current board is: <6:1><1:1><1:4><4:0><0:0><0:5><5:5><5:1><1:0><0:3><3:5>
   Alice plays <4:6> to connect it to tile <6:1> on the board
   The current board is: <4:6><6:1><1:1><1:4><4:0><0:0><0:5><5:5><5:1><1:0><0:3><3:5>
   Bob plays <4:4> to connect it to tile <4:6> on the board
   The current board is: <4:4><4:6><6:1><1:1><1:4><4:0><0:0><0:5><5:5><5:1><1:0><0:3><3:5>
   Alice plays <3:4> to connect it to tile <4:4> on the board
   The current board is: <3:4><4:4><4:6><6:1><1:1><1:4><4:0><0:0><0:5><5:5><5:1><1:0><0:3><3:5>
   Player Alice has won!
```
---

## How to use it
You’ll start by accessing the root path of the project `/public_html/index.php`
The application will display the game process between players.

---

## Installation of the application
1. Clone project from Bitbucket: `git clone https://valentinstancescu@bitbucket.org/valentinstancescu/domino-game.git`
2. Make sure the server where project is deployed runs: `PHP >=5.6`, `Apache2`
3. Php modules that must to be enabled: `bcmath`
4. Application also requires `COMPOSER` to be installed.
5. Copy files on the server, making `public_html` folder as root of the project.
6. Run `composer install` to generate autoload file

---

## Folders/files and their roles

Short description of folders and files of the project.

1. `\app\init.php` - holds the files that initialize the common data for all application
2. `\public_html` - holds entry point / public files of the project
3. `\src` - holds sources of the application
4. `\src\lib` - holds folders and files for application logic
5. `\src\lib\Domino` - contains service class that expose methods for playing game

- No external libraries were used in this project
