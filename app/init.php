<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('xdebug.max_nesting_level', 256);
require_once(dirname(__FILE__) . '/../vendor/autoload.php');