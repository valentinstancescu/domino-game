<?php

use lib\Domino\Deck;
use lib\Domino\GameActionsLogger;
use lib\Domino\GameService;
use lib\Domino\GameTable;
use lib\Domino\Player;

require_once(dirname(__FILE__) . '/../app/init.php');

$domino = new GameService(new Deck(), new Player(), new GameTable(), new GameActionsLogger());
$domino->playGame();