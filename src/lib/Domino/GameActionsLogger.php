<?php

namespace lib\Domino;

/**
 * Class GameActionsLogger - logs actions that happens during the game
 */
class GameActionsLogger
{
    private $loggedGameActions = [];

    /**
     * @return array
     */
    public function getGameActions()
    {
        return $this->loggedGameActions;
    }

    /**
     * @param string $gameAction
     *
     * @return void;
     */
    public function logGameAction($gameAction)
    {
        $this->loggedGameActions[] = $gameAction;

        return;
    }
}
