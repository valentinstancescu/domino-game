<?php

namespace lib\Domino;

/**
 * Class GameService - class that has the logic of the entire game
 */
class GameService
{
    /** @var Deck */
    private $deck;

    /** @var Player */
    private $player;

    /** @var GameTable */
    private $gameTable;

    /** @var GameActionsLogger */
    private $actionsLogger;

    /**
     * GameController constructor - inject needed services that are needed to play the game
     *
     * @param Deck $deck
     * @param Player $player
     * @param GameTable $gameTable
     * @param GameActionsLogger $actionsLogger
     */
    public function __construct(Deck $deck, Player $player, GameTable $gameTable, GameActionsLogger $actionsLogger)
    {
        $this->deck = $deck;
        $this->player = $player;
        $this->gameTable = $gameTable;
        $this->actionsLogger = $actionsLogger;
    }

    /**
     * Main method that plays the Domino game
     *
     * @return void
     */
    public function playGame()
    {
        $this->prepareTilesToStart();
        $this->initTable();
        $this->playOneHand();
        $this->displayGameActions();

        return;
    }

    /**
     * Prepares data for starting the game
     *
     * @return void
     */
    private function prepareTilesToStart()
    {
        //generates the tiles and put them on a deck(pile)
        $generatedDeck = $this->deck->generateDeck();

        //shuffle tiles
        shuffle($generatedDeck);

        //assign tiles to be distributed to players
        $players = $this->player->getPlayers();
        shuffle($players);

        $tilesForPlayers = array_splice($generatedDeck, 0, bcmul(Player::TILES_FOR_PLAYER, count($players)));
        $this->player->assignTilesToPlayers($tilesForPlayers);

        //sets the first player
        $this->player->setCurrentPlayer($players[0]);

        //set the deck with the remaining tiles
        $this->deck->setRemainingTilesOnDeck($generatedDeck);

        return;
    }

    /**
     * @return void
     */
    private function initTable()
    {
        //set first tile on the table from the tiles remained on the deck
        $firstTileOnTable = $this->deck->extractTileFromDeck();
        $this->gameTable->addTileOnTable($firstTileOnTable, GameTable::TILE_POSITION_LEFT);

        $this->actionsLogger->logGameAction(
            sprintf('Game starting with first tile: %s', $this->formatTile($firstTileOnTable))
        );

        return;
    }

    /**
     * Plays a hand of the game
     */
    private function playOneHand()
    {
        $currentPlayer  = $this->player->getCurrentPlayer();
        $tableHeads = $this->gameTable->getTableHeads();
        $playerTiles = $this->player->getPlayerTilesByName($currentPlayer);
        $tileIsPlayed = true;

        //check tiles the current player if one of them can be connected to the table
        foreach ($playerTiles as $playersTilePosition => $tile) {
            $tileIsPlayed = $this->checkIfTileGoesOnTable($tile, $tableHeads, $currentPlayer, $playersTilePosition);

            if ($tileIsPlayed) {
                $this->actionsLogger->logGameAction(
                    sprintf('The current board is: %s', $this->formatTilesOnTable())
                );

                break;
            }
        }

        if (!$tileIsPlayed && !empty($this->deck->getTilesOnDeck())) { // draw a tile and 'put it in hand'. Same player will continue the game
            $extractedTile                 = $this->deck->extractTileFromDeck();
            $this->player->addTileToPlayer($currentPlayer, $extractedTile);

            $this->actionsLogger->logGameAction(
                sprintf("%s cant't play, drawing tile %s", $currentPlayer, $this->formatTile($extractedTile))
            );
        } else { // Pass the hand to the other player
            $players = array_flip($this->player->getPlayers());
            unset($players[$currentPlayer]);
            $this->player->setCurrentPlayer(key($players));
        }


        if (!empty($this->player->getPlayerTilesByName($currentPlayer))) { // If current player has more tiles in hand, will continue to play
            $this->playOneHand();
        } else { //if his/her hand is empty is the winner
            $this->actionsLogger->logGameAction(
                sprintf('Player %s has won!', $currentPlayer)
            );
        }
    }

    /**
     * @param array $tile
     * @param array $tableHeads
     * @param string $currentPlayer
     * @param int $playersTilePosition
     *
     * @return bool
     */
    private function checkIfTileGoesOnTable(array $tile, array $tableHeads, $currentPlayer, $playersTilePosition)
    {
        if ($tile[0] === $tableHeads[0]) {
            // Tile will go at the left at the table but flipped
            $this->setTileAsPlayed($tile, $currentPlayer, $playersTilePosition, GameTable::TILE_POSITION_LEFT);
            $this->gameTable->addTileOnTable(array_reverse($tile), GameTable::TILE_POSITION_LEFT);

            return true;
        } elseif ($tile[0] === $tableHeads[1]) {
            // Tile will go at the right of the table
            $this->setTileAsPlayed($tile, $currentPlayer, $playersTilePosition, GameTable::TILE_POSITION_RIGHT);
            $this->gameTable->addTileOnTable($tile, GameTable::TILE_POSITION_RIGHT);

            return true;
        } elseif ($tile[1] === $tableHeads[0]) {
            // Tile will go on the left of the table
            $this->setTileAsPlayed($tile, $currentPlayer, $playersTilePosition, GameTable::TILE_POSITION_LEFT);
            $this->gameTable->addTileOnTable($tile, GameTable::TILE_POSITION_LEFT);

            return true;
        }  elseif ($tile[1] === $tableHeads[0]) {
            // Tile will go on the right at the table but flipped
            $this->setTileAsPlayed($tile, $currentPlayer, $playersTilePosition, GameTable::TILE_POSITION_RIGHT);
            $this->gameTable->addTileOnTable(array_reverse($tile), GameTable::TILE_POSITION_RIGHT);

            return true;
        }

        return false;
    }

    /**
     * @param array $playedTile
     * @param string $currentPlayer
     * @param int $tilePosition
     * @param string $tileOnTablePosition
     */
    private function setTileAsPlayed(array $playedTile, $currentPlayer, $tilePosition, $tileOnTablePosition)
    {
        $tileToConnect =  $this->gameTable->getTableLatestTileByPosition($tileOnTablePosition);

        $this->actionsLogger->logGameAction(
            sprintf(
                '%s plays %s to connect it to tile %s on the board',
                $currentPlayer,
                $this->formatTile($playedTile),
                $this->formatTile($tileToConnect)
            )
        );

        $this->player->removeTileFromPlayer($currentPlayer, $tilePosition);

        return;
    }

    /**
     * @param array $tileValues
     *
     * @return string
     */
    private function formatTile(array $tileValues)
    {
        return sprintf('<%s:%s>', $tileValues[0], $tileValues[1]);
    }

    /**
     * @return string
     */
    private function formatTilesOnTable()
    {
        $tilesOnTable = '';
        foreach ($this->gameTable->getTilesOnTable() as $tileOnTable) {
            $tilesOnTable .= $this->formatTile($tileOnTable);
        }

        return $tilesOnTable;
    }

    /**
     * @return void
     */
    private function displayGameActions()
    {
        $separator = (php_sapi_name() === 'cli') ? PHP_EOL : '<br/>';

        foreach ($this->actionsLogger->getGameActions() as $gameAction) {
            echo $gameAction . $separator;
        }

        return;
    }
}
