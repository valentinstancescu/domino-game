<?php

namespace lib\Domino;

/**
 * Class Player - handles information about players of the game
 */
class Player
{
    const TILES_FOR_PLAYER = 7;

    /** @var array */
    private $players = ['Alice', 'Bob'];

    /** @var string */
    private $currentPlayer;

    /** @var array - represents an array with tiles that each player has in his/her hand */
    private $playerTiles = [];

    /**
     * From the generated deck of tiles, every player receives 7 tiles
     *
     * @param array $generatedDeck
     *
     * @return array
     */
    public function assignTilesToPlayers(array $generatedDeck)
    {
        foreach ($this->players as $player) {
            $this->playerTiles[$player] = array_splice($generatedDeck, 0, self::TILES_FOR_PLAYER);
        }

        return $this->playerTiles;
    }

    /**
     * @return array
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * @return string
     */
    public function getCurrentPlayer()
    {
        return $this->currentPlayer;
    }

    /**
     * @param string $currentPlayer
     */
    public function setCurrentPlayer($currentPlayer)
    {
        $this->currentPlayer = $currentPlayer;
    }

    /**
     * Gets the tiles that a player has in his/her hand
     *
     * @param string $playerName
     *
     * @return array
     */
    public function getPlayerTilesByName($playerName)
    {
        return $this->playerTiles[$playerName];
    }

    /**
     * Add new extracted tile from deck to player's hand
     *
     * @param string $playerName
     * @param array $extractedTile
     *
     * @return void
     */
    public function addTileToPlayer($playerName, $extractedTile)
    {
        array_push($this->playerTiles[$playerName], $extractedTile);

        return;
    }

    /**
     * Removes tile from player's hand when it is played on the table
     *
     * @param string $playerName
     * @param int $playedTilePosition
     *
     * @return void
     */
    public function removeTileFromPlayer($playerName, $playedTilePosition)
    {
        unset($this->playerTiles[$playerName][$playedTilePosition]);

        return;
    }
}
