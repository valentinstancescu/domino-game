<?php

namespace lib\Domino;

/**
 * Class GameTable - represents the game table and offers methods to handle tiles on table
 */
class GameTable
{
    const TILE_POSITION_LEFT = 'left';
    const TILE_POSITION_RIGHT = 'right';

    /** @var array */
    private $tilesOnTable = [];

    /**
     * Gets current tiles on table
     *
     * @return array
     */
    public function getTilesOnTable()
    {
        return $this->tilesOnTable;
    }

    /**
     * Adds new tile on table by putting it at the right or at the left of the deck
     *
     * @param array $tile
     * @param string $tilePosition
     *
     * @return void;
     */
    public function addTileOnTable(array $tile, $tilePosition)
    {
        switch ($tilePosition) {
            case self::TILE_POSITION_RIGHT:
                array_push($this->tilesOnTable, $tile);
                break;
            case self::TILE_POSITION_LEFT:
                array_unshift($this->tilesOnTable, $tile);
                break;
        }

        return;
    }

    /**
     * Returns the latest numbers from the tiles that are placed by the both heads of the deck
     * For the deck <4:3><3:1><1:0><0:1> will return [4,1]
     *
     * @return array
     */
    public function getTableHeads()
    {
        $tableHeadTiles = $this->getTableHeadTiles();

        return [$tableHeadTiles[self::TILE_POSITION_LEFT][0], $tableHeadTiles[self::TILE_POSITION_RIGHT][1]];
    }

    /**
     * Returns the latest tile from the deck by specified position
     * For the deck <4:3><3:1><1:0><0:1> will return [4,3] for left and [0,1] for right
     *
     * @param string $tilePosition
     *
     * @return array
     */
    public function getTableLatestTileByPosition($tilePosition)
    {
        $tableHeadTiles = $this->getTableHeadTiles();

        return $tableHeadTiles[$tilePosition];
    }

    /**
     * @return array
     */
    private function getTableHeadTiles()
    {
        $leftTileFromTable = reset($this->tilesOnTable);
        $rightTileFromTable = end($this->tilesOnTable);
        reset($this->tilesOnTable);

        return [
            self::TILE_POSITION_LEFT => $leftTileFromTable,
            self::TILE_POSITION_RIGHT => $rightTileFromTable,
        ];
    }
}