<?php

namespace lib\Domino;

/**
 * Class Deck - handles information about the deck(pile) of tiles.
 */
class Deck
{
    /** @var array */
    private $tilesOnDeck = [];

    /**
     * Generates an array representing the tiles of the game, with corresponding numbers as values from an array
     * representing a single tile
     *
     * @return array
     */
    public function generateDeck()
    {
        $generatedTiles = [];

        for ($i = 0; $i <= 6; $i++) {
            for ($j = $i; $j <= 6; $j++) {
                $generatedTiles[] = [$i, $j];
            }
        }

        return $generatedTiles;
    }

    /**
     * Gets the tiles that were not distributed to players at the beginning of the game
     *
     * @return array
     */
    public function getTilesOnDeck()
    {
        return $this->tilesOnDeck;
    }

    /**
     * Sets the tiles that were not distributed to players at the beginning of the game
     *
     * @param array $tilesOnDeck
     *
     * @return void
     */
    public function setRemainingTilesOnDeck($tilesOnDeck)
    {
        $this->tilesOnDeck = $tilesOnDeck;

        return;
    }

    /**
     * Extract a tile from the deck when the player does not have a good tile in his hand to be placed on the table
     *
     * @return array
     */
    public function extractTileFromDeck()
    {
        return array_splice($this->tilesOnDeck, 0, 1)[0];
    }
}
